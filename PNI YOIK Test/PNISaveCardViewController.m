//
//  PNISaveCardViewController.m
//  PNI YOIK Test
//
//  Created by Grant Benvenuti on 3/03/2014.
//  Copyright (c) 2014 Payment Network International Pty Ltd. All rights reserved.
//

#import "PNISaveCardViewController.h"

@interface PNISaveCardViewController ()
@property (weak, nonatomic) IBOutlet UIWebView *savecardwebview;

@end

@implementation PNISaveCardViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }

    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    self.savecardwebview.delegate = self;
    
    //load url into webview
    NSString *baseURL = @"https://pay.yoik.net/form";
    NSString *merchant = @"test0";
    NSString *userId = @"userx";
    NSString *bearer = @"aewuWKCsXcVPhD4eEFFBxBDrXr";
    NSString *redirect = @"http://myappreturn"; // fictional url to watch for, called on cancel and success

    NSString *encodedString = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(
                                                                                   NULL,
                                                                                   (CFStringRef)redirect,
                                                                                   NULL,
                                                                                   (CFStringRef)@"!*'();:@&=+$,/?%#[]",
                                                                                   kCFStringEncodingUTF8 ));
    
    NSString *strURL = [NSString stringWithFormat:@"%@?merchantid=%@&userid=%@&bearer=%@&redirecturl=%@", baseURL, merchant, userId, bearer, encodedString];
    NSLog(@"%@", strURL);
    NSURL *url = [NSURL URLWithString:strURL];
    NSURLRequest *urlRequest = [NSURLRequest requestWithURL:url];
    [self.savecardwebview loadRequest:urlRequest];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType {
    
    NSLog(@"%@", [[request URL] absoluteString]);
    
    NSLog(@"%@",[[request URL] query]);
    
    NSString *URLString = [[request URL] absoluteString];
    if ([URLString rangeOfString:@"http://myappreturn"].location == NSNotFound) {
        // not the url we're looking for
        
    } else {
        // we've returned to our fictional url so process the query string and return
        NSDictionary *dict = [self parseQueryString:[[request URL] query]];
        NSLog(@"query dict: %@", dict);
        
        [self performSegueWithIdentifier: @"BackToMain" sender: self];
    }
    return YES;
}

- (NSDictionary *)parseQueryString:(NSString *)query {
    NSMutableDictionary *dict = [[NSMutableDictionary alloc] initWithCapacity:6];
    NSArray *pairs = [query componentsSeparatedByString:@"&"];
    
    for (NSString *pair in pairs) {
        NSArray *elements = [pair componentsSeparatedByString:@"="];
        NSString *key = [[elements objectAtIndex:0] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        NSString *val = [[elements objectAtIndex:1] stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
        
        [dict setObject:val forKey:key];
    }
    return dict;
}

@end
